﻿using UnityEngine.Events;

namespace Skydra.Helper.Data
{
    public class InputData
    {
        private object _value;
        public object Value => _value;
        
        public UnityAction<object> OnChange;

        public InputData(object v)
        {
            _value = v;
        }
        
        public void Set(object v)
        {
            if (_value == v)
                return;
            
            _value = v;
            OnChange?.Invoke(v);
        }
    }
}