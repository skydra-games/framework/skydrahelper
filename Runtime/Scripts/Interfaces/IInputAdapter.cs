﻿using UnityEngine;
using Skydra.Helper.Data;

namespace Skydra.Helper
{
    public interface IInputAdapter
    {
        public void Initialize();
        public void SetInput<TInput>(string inputID, TInput value);
        public TInput GetInput<TInput>(string inputID);
        public bool TryGetInputData(string inputID, out InputData data);
    }
}